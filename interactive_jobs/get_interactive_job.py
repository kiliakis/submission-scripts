#!/usr/bin/env python

import argparse
import sys
import subprocess
import os

this_directory = os.path.dirname(os.path.realpath(__file__))


parser = argparse.ArgumentParser(
    description='Submit requests for interactive jobs. ',
    usage='{} --help'.format(sys.argv[0]))


parser.add_argument('-d', '--device', type=str, choices=['cpu', 'cpuintel', 'cpuamd', 'gpu', 'gput4', 'gpuv100', 'gpua100'],
                    default='cpu',
                    help='Specify the device you want to access. Default: cpu')

parser.add_argument('-t', '--time', type=int, default=3600, 
                    help='Max duration of your session in seconds. Default: 3600')

parser.add_argument('-c', '--cores', type=int, default=4, 
                    help='Number of cores of your request. Default: 4')

parser.add_argument('-m', '--memory', type=int, default=8, 
                    help='RAM memory size of your request in GB. Default: 8')


def main():
    device_sub_dict = {
        'cpu': f'{this_directory}/cpu-any.sub',
        'cpuintel': f'{this_directory}/cpu-intel.sub',
        'cpuamd': f'{this_directory}/cpu-amd.sub',
        'gpu': f'{this_directory}/gpu-any.sub',
        'gput4': f'{this_directory}/gpu-t4.sub',
        'gpuv100': f'{this_directory}/gpu-v100.sub',
        'gpua100': f'{this_directory}/gpu-a100.sub',
    }

    args = parser.parse_args()

    submit_file = device_sub_dict[args.device]

    # RAM memory should be at least 2GB per core
    memory = max(args.memory, 2 * args.cores)
    
    submit_options = ['condor_submit -interactive',
                     f'-a +MaxRuntime={args.time}', 
                     f'-a request_cpus={args.cores}', 
                     f'-a request_memory={memory}G',
                    #  f'{device_requirements_dict[args.device]}',
                     f'-file {submit_file}']
    
    submit_options = ' '.join(submit_options)
    print('Submit command:')
    print(submit_options)
    subprocess.run(submit_options, shell=True)


if __name__ == '__main__':
    main()
