'''
Example input for simulation of acceleration
No intensity effects

:Authors: **Helga Timko, Konstantinos Iliakis**
'''
#  General Imports
from builtins import range
import numpy as np
import os
import argparse
import yaml

#  BLonD Imports
from blond.input_parameters.ring import Ring
from blond.input_parameters.rf_parameters import RFStation
from blond.trackers.tracker import RingAndRFTracker
from blond.beam.beam import Beam, Proton
from blond.beam.distributions import bigaussian
from blond.beam.profile import CutOptions, FitOptions, Profile
from blond.monitors.monitors import BunchMonitor
from blond.plots.plot import Plot

import matplotlib as mpl
mpl.use('Agg')

parser = argparse.ArgumentParser(description='Run main file.')
parser.add_argument('-c', '--config', type=str, help='Yaml input configuration file.')
args = parser.parse_args()


# Simulation parameters -------------------------------------------------------
# Bunch parameters
N_b = 1e9           # Intensity
N_p = 50000         # Macro-particles
tau_0 = 0.4e-9          # Initial bunch length, 4 sigma [s]

# Machine and RF parameters
C = 26658.883        # Machine circumference [m]
p_i = 450e9         # Synchronous momentum [eV/c]
p_f = 460.005e9      # Synchronous momentum, final
h = 35640            # Harmonic number
V = 6e6                # RF voltage [V]
dphi = 0             # Phase modulation/offset
gamma_t = 55.759505  # Transition gamma
alpha = 1./gamma_t/gamma_t        # First order mom. comp. factor

# Tracking details
N_t = 2000           # Number of turns to track
dt_plt = 200         # Time steps between plots

output_dir = './'

if args.config is not None:
    # Load config file
    with open(args.config,'r') as inputfile:
        params = yaml.load(inputfile, Loader=yaml.FullLoader)

    # Overwrite default parameters
    N_b = params['N_b']
    N_t = params['N_t']
    p_i = params['p_i']
    p_f = params['p_f']
    output_dir = params.get('output_dir', './')

os.makedirs(output_dir, exist_ok=True)


# Simulation setup ------------------------------------------------------------
print("Setting up the simulation...")
print("")


# Define general parameters
ring = Ring(C, alpha, np.linspace(p_i, p_f, N_t+1), Proton(), N_t)

# Define beam and distribution
beam = Beam(ring, N_p, N_b)


# Define RF station parameters and corresponding tracker
rf = RFStation(ring, [h], [V], [dphi])
long_tracker = RingAndRFTracker(rf, beam)


bigaussian(ring, rf, beam, tau_0/4, reinsertion=True, seed=1)

# Need slices for the Gaussian fit
profile = Profile(beam, CutOptions(n_slices=100),
                FitOptions(fit_option='gaussian'))

# Define what to save in file
bunchmonitor = BunchMonitor(ring, rf, beam, 
                os.path.join(output_dir, 'output_data'), Profile=profile)

format_options = {'dirname': output_dir}
plots = Plot(ring, rf, beam, dt_plt, N_t, 0, 0.0001763*h,
            -400e6, 400e6, xunit='rad', separatrix_plot=True,
            Profile=profile, h5file=os.path.join(output_dir, 'output_data'),
            format_options=format_options)

# Accelerator map
map_ = [long_tracker] + [profile] + [bunchmonitor] + [plots]
print("Map set")
# Tracking --------------------------------------------------------------------
for i in range(1, N_t+1):

    # Plot has to be done before tracking (at least for cases with separatrix)
    if (i % dt_plt) == 0:
        print("Outputting at time step %d..." % i)
        print("   Beam momentum %.6e eV" % beam.momentum)
        print("   Beam gamma %3.3f" % beam.gamma)
        print("   Beam beta %3.3f" % beam.beta)
        print("   Beam energy %.6e eV" % beam.energy)
        print("   Four-times r.m.s. bunch length %.4e s" % (4.*beam.sigma_dt))
        print("   Gaussian bunch length %.4e s" % profile.bunchLength)
        print("")

    # Track
    for m in map_:
        m.track()

    # Define losses according to separatrix and/or longitudinal position
    beam.losses_separatrix(ring, rf)
    beam.losses_longitudinal_cut(0., 2.5e-9)

print("Done!")


